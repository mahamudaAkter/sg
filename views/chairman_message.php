<?php
	include('header.php');
?>
	<div class="bcumb" style="background-image: url('../resource/img/banner.jpg')">
		<div class="overlay">
			<div class="container text-center">
				<div class="bcumbarea">
					<h4>Message From Chairman</h4>
					<p >Alumni needs enable you to harness the power of your alumni network. Whatever may be the need.</p>
					<a href="#" class="btn btn-default abttop">Let's See</a>
				</div>
			</div>
		</div>
	</div>
	<div class="abtcontent">
		<div class="container">
			
			<div class="area">
				<h4 class="text-right">2018</h4>
				<div style="float:right;" class="col-md-5"><img style="width:70%; height:auto;" src="../resource/img/principle.png" alt="Image" class="img img-responsive"/></div>

				<p>
					<br>
					<span class="firstcharacter">W</span>e welcome you all to the new website of The Chittagong University English Alumni Association. From now on, you will be regularly updated about the activities of the association and the English Department here. If you have received any degree, such as BA (Honours), MA, MPhil, or PhD from this department, you are one of our valued and proud alumni. We want this site to become a vibrant platform for the alumni across the globe to share their success stories which would inspire our existing students, and guide them to choose their professional pathways. Let’s grow together to serve the nation better.
				</p>
			</div>
			
		</div>
	</div>
	
	
<?php
	include('footer.php');
?>