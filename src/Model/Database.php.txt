<?php

namespace App\Model;
use PDO, PDOException;

class Database
{
    public $conn;
    public $username="bsblcom_accounts";
    public $password="";
    public $DBH;

    public function __construct()
    {

        try{

            //$this->conn = new PDO("mysql:host=localhost;dbname=olineit_salabir", $this->username, $this->password);
            $this->DBH = new PDO('mysql:host=localhost;dbname=dbname', "username", "password");
            $this->DBH->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );


        }
        catch(PDOException $error){

           echo "Database Error: ". $error->getMessage();
        }



    }


}