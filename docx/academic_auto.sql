-- phpMyAdmin SQL Dump
-- version 4.4.15.9
-- https://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Apr 24, 2019 at 01:53 PM
-- Server version: 5.6.37
-- PHP Version: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `academic_auto`
--

-- --------------------------------------------------------

--
-- Table structure for table `members`
--

CREATE TABLE IF NOT EXISTS `members` (
  `id` int(11) NOT NULL,
  `password` varchar(20) DEFAULT NULL,
  `fullName` varchar(200) DEFAULT NULL,
  `fatherName` varchar(200) DEFAULT NULL,
  `motherName` varchar(200) DEFAULT NULL,
  `addressLine1` varchar(300) DEFAULT NULL,
  `addressLine2` varchar(300) DEFAULT NULL,
  `city` varchar(20) DEFAULT NULL,
  `state` varchar(20) DEFAULT NULL,
  `postcode` varchar(20) DEFAULT NULL,
  `country` varchar(15) DEFAULT NULL,
  `email` varchar(40) NOT NULL,
  `phoneNumber` varchar(15) DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `gender` varchar(6) DEFAULT NULL,
  `passport` varchar(30) DEFAULT NULL,
  `birthcert` varchar(30) DEFAULT NULL,
  `passport_file` varchar(30) DEFAULT NULL,
  `picture` varchar(30) DEFAULT NULL,
  `brc` varchar(30) DEFAULT NULL,
  `cv` varchar(30) DEFAULT NULL,
  `comment` varchar(300) DEFAULT NULL,
  `soft_delete` varchar(3) DEFAULT 'No'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `members`
--

INSERT INTO `members` (`id`, `password`, `fullName`, `fatherName`, `motherName`, `addressLine1`, `addressLine2`, `city`, `state`, `postcode`, `country`, `email`, `phoneNumber`, `dob`, `gender`, `passport`, `birthcert`, `passport_file`, `picture`, `brc`, `cv`, `comment`, `soft_delete`) VALUES
(1, NULL, 'Kazi Sala Uddin', 'Kazi Roushaner Zaman', 'Monoara Begum', '57 Main Road', 'Proshanti R/A', 'Chattogram', 'N/A', '4000', 'BGD', 'amisalabir@gmail.com', '01714130077', '2019-04-24', 'Studen', '843587232', '2342535523', NULL, NULL, NULL, NULL, NULL, 'No');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `members`
--
ALTER TABLE `members`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `members`
--
ALTER TABLE `members`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
